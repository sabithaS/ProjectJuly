package Project_Aug;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import testNg.Project_Methods;

public class Facebook extends Project_Methods {
	@BeforeClass

	public void testdata() {


		testCasename="TC001";
		testCaseDescription="login facebook";
		author="Sabitha";
		category="functional";
	}
	@Test

	public void faceBook() {

		try {
			startApp("Chrome", "https://www.facebook.com/");

			//to login
			WebElement username = locateElement("id", "email");
			type(username, "muruganmuthukumari@gmail.com");
			WebElement password = locateElement("id", "pass");
			type(password, "rameshkumari@10");
			WebElement login = locateElement("xpath", "//input[@value='Log In']");
			click(login);

			//Clicking on search
			WebElement search = locateElement("xpath", "//input[@data-testid='search_input']");

			type(search, "Testleaf");

			//clicking on serach icon
			WebElement searchIcon = locateElement("xpath", "(//button[@type='submit']/i)[1]");
			click(searchIcon);

			//Getting the text of like
			WebElement likeButton = locateElement("xpath", "(//button[@type='submit']/i)[2]");
			if(likeButton.isSelected())
			{
				System.out.println("like button is selected");
			}		
			else {
				click(likeButton);
				System.out.println("Like button is already clicked");
			}

			//clicking on the link test lead
			WebElement testleaf = locateElement("xpath", " (//div[text()='TestLeaf'])[1]");
			click(testleaf);
			//webdriverwait(testleaf);

			//Getting the title and verifying the title details
			verifyTitle("(94) TestLeaf - Home");
			//verifying the like of the page
			WebElement peoplelikethis = locateElement("xpath", "//div[contains(text(),'people like this')]");
			String text = getText(peoplelikethis);
			System.out.println(text.replaceAll("[^0-9]",""));


		} catch (Exception e) {
			System.out.println("something went wrong");
		}











	}

}
