package Project_Aug;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;

public class Fix_Bug {

	public static void main(String[] args) throws InterruptedException {



		// launch the browser
		//System.setProperty("Webdriver.Chrome.driver","./Drivers/chromedriver.exe");
		System.setProperty("webdriver.chrome.driver", "./Drivers/chromedriver.exe");

		ChromeDriver driver = new ChromeDriver();
		driver.get("https://www.myntra.com/");
		driver.manage().window().maximize();

		// Mouse Over on Men
		Actions builder = new Actions(driver);
		builder.moveToElement(driver.findElementByLinkText("Men")).perform();

		// Click on Jackets
		driver.findElementByXPath("//a[@href='/men-jackets']").click();


		// Find the count of Jackets
		String leftCount = 
				driver.findElementByXPath("//label[@class='common-customCheckbox vertical-filters-label']")
				.getText()
				.replaceAll("[^0-9]", "");
		System.out.println(leftCount);


		// Click on Jackets and confirm the count is same
		driver.findElementByXPath("//label[text()='Jackets']").click();

		// Wait for some time
		Thread.sleep(5000);

		// Check the count
		String rightCount = 
				driver.findElementByXPath("//span[@class='horizontal-filters-sub']")
				.getText()
				.replaceAll("[^0-9]", "");
		System.out.println(rightCount);

		// If both count matches, say success
		if(leftCount.equals(rightCount)) {
			System.out.println("The count matches on either case");
		}else {
			System.err.println("The count does not match");
		}

		// Click on Offers
		driver.findElementByXPath("//h4[text()='Offers']").click();

		// Find the costliest Jacket
		List<WebElement> productsPrice = driver.findElementsByXPath("//span[@class='product-discountedPrice']");
		List<String> onlyPrice = new ArrayList<String>();


		for (WebElement productPrice : productsPrice) {
			onlyPrice.add(productPrice.getText().replaceAll("[^0-9]", ""));
		}
		System.out.println(onlyPrice);
		// Sort them 
		String max = Collections.max(onlyPrice);

		// Find the top one
		System.out.println("maximum price is " + max);


		/*//filter with allen solly

		driver.findElementByXPath(" //div[@class='brand-more']").click();

		//filtering with allen solly
		driver.findElementByXPath("(//label[@class=' common-customCheckbox'])[2]").click();

		Thread.sleep(2000);
		//close the popup
		driver.findElementByXPath("//span[@class='myntraweb-sprite FilterDirectory-close sprites-remove']").click();

		Thread.sleep(2000);*/


		// Print Only Allen Solly Brand Minimum Price
		/*WebElement results = driver.findElementByXPath(" //ul[@class='results-base']");

		// Find the costliest Jacket
		List<WebElement> allenSollyPrices = driver.findElementsByXPath("//span[@class='product-discountedPrice']");

		List<String> allensolly = new ArrayList<String>();

		for (WebElement eachprice : allenSollyPrices) {
			allensolly.add(eachprice.getText().replaceAll("[^0-9]", ""));
		}
		System.out.println(onlyPrice);

		// Get the minimum Price 
		String min = Collections.min(onlyPrice);

		// Find the lowest priced Allen Solly
		System.out.println(min);
		 */
		//driver.close();


	}

}


