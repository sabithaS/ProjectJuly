package Project_Aug;

import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;

import wdMethods.SeMethods;

public class Methods_Using_Testng extends SeMethods{

	


	@BeforeMethod(groups="common")
	
	//@Parameters({"browser","url","username","password"})
	
	public void login () {

		startApp("Chrome", "http://leaftaps.com/opentaps");

		WebElement username = locateElement("id", "username");	
		//to send the value for a element 
		type(username, "demosalesmanager");
		WebElement password = locateElement("id", "password");
		type(password, "crmsfa");
		//To click the login button
		WebElement login = locateElement("class", "decorativeSubmit");
		click(login);
		//to click the crmsfa link
		WebElement crmsfa = locateElement("link", "CRM/SFA");
		click(crmsfa);
	}
	
	@AfterMethod(groups="common")
	public void logout() {
		closeBrowser();
	}

}
