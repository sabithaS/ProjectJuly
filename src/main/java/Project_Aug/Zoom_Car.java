package Project_Aug;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import testNg.Project_Methods;
import wdMethods.SeMethods;
import week3_day1.FindElements;

public class Zoom_Car extends SeMethods {

	@BeforeClass

	public void testdata() {


		testCasename="TC001";
		testCaseDescription="Zoom Car";
		author="Sabitha";
		category="functional";
	}


	@Test

	public void highestPrice() throws InterruptedException {

		startApp("chrome", "https://www.zoomcar.com/chennai");
		Thread.sleep(3000);
		//Clicking on popular link
		WebElement search = locateElement("class", "search");
		click(search);
		//clicking on anyone of the popular points
		WebElement popularpoint = locateElement("xpath", "//div[@class='items']");
		click(popularpoint);
		//clicking on Next button

		WebElement next = locateElement("class", "proceed");
		clickWithNoSnap(next);

		//Spcifying the start day as tomorrow day
		WebElement day = locateElement("xpath", "//div[@class='day full']");
		click(day);
		//Again clicking on next button
		WebElement next1 = locateElement("class", "proceed");
		click(next1);

		//confirming the start date
		/*WebElement startday = locateElement("xpath", "//div[@class='days']/div[1]");
		verifyExactText(startday, "22");
		 */
		//confirming the start day and time
		WebElement startdate = locateElement("xpath", "//div[@class='label time-label']");

		verifyExactText(startdate, "Tue 21 Aug, 2018 22:30");

		//Clicking on done button
		WebElement done = locateElement("class", "proceed");
		click(done);

		//capturing the number of results displayed

		WebElement results = locateElement("xpath", "//div[@class='car-list-layout']");

		webdriverwait(results);

		//capturing each price
		List<WebElement> Allprices = driver.findElementsByXPath("//div[@class='price']");


		List<String> eachprice=new ArrayList<String>();

		for (WebElement eachprices : Allprices) {

			eachprice.add(eachprices.getText().replaceAll("[^0-9]", ""));
		}

		System.out.println(eachprice);
		String max = Collections.max(eachprice);

		System.out.println(max);
		//Entering the model name of maximum amount car	
		/*int size = eachprice.size();
		String highestprice = eachprice.get(size-1);

		System.out.println("Highest price is " + highestprice);

		 */	
		String carname = driver.findElementByXPath("//div[contains(text(),\"+max+\")]/preceding::h3\"").getText();
		System.out.println(carname);
	}





}
