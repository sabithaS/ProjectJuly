package Week2_Day1;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ArrayList_ElectronicShop {

	public static void main(String[] args) {
		//Create a list
		List<String> shop =new ArrayList<String>();


		//adding the items

		shop.add("ONIDA");
		shop.add("SAMSUNG");
		shop.add("ONIDA");
		int size=shop.size();
		System.out.println(size);

		//confirm list maintain in the order

		for(String eachtv:shop) {

			System.out.println(eachtv);
		}


		//removing the last added
		shop.remove(2);
		int size1=shop.size();


		System.out.println(size1);

		//Alphabetic order
		Collections.sort(shop);

		System.out.println(shop);





	}

}
