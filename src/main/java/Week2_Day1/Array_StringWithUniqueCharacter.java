

package Week2_Day1;

import java.util.LinkedHashSet;
import java.util.Set;
import java.util.TreeSet;

public class Array_StringWithUniqueCharacter {

	public static void main(String[] args) {
		
		//Stirng declarioon and initiation

		String name="Cognizant India";
		//Converting the string into array


		char[] charArray=name.toCharArray();
		
		//unique character we are using set

		Set<Character> unique=new LinkedHashSet <Character>();
		
		//store the element in index

		for(int i=0;i<charArray.length;i++) {

			unique.add(charArray[i]);
		}
		System.out.print(charArray);
		
	//to print the each array details  we are using to for each 

		for (char c : unique) {

			System.out.print(c);

		}

	}
}


