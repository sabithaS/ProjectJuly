package Week2_Day1;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
public class Map_LastModel {

	public static void main(String[] args) {
		// map
		Map<String, Integer>  allMobiles = new HashMap<String, Integer>();
		// Add items to a map
		allMobiles.put("Samsung S9", 1);
		allMobiles.put("IPhone X", 2);
		allMobiles.put("One Plus 6", 1);
		System.out.println(allMobiles.size());
		// Update map
		allMobiles.put("Samsung S9", 3);
		allMobiles.put("Samsung S9", 0);
		allMobiles.remove("Samsung S9");
		// Get a value 
		if (allMobiles.containsKey("Samsung S9")) {
			Integer integer = allMobiles.get("Samsung S9");
			System.out.println(integer);

		}else {
			System.out.println("Key does not exists");
		}
		// combination of key & value is EntrySet
		for (Entry<String, Integer> mobile : allMobiles.entrySet()) {
			System.out.println(mobile.getKey());
			System.out.println(mobile.getValue());

		}


	}

}
