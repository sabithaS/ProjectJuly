package Week2_Day1;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegEx_Example {

	public static void main(String[] args) {

		String text="a";

		//Pattern
		String pattern = "[abc]";
		Pattern p = Pattern.compile(pattern);
		Matcher matcher = p.matcher(text);
		System.out.println(matcher.matches());
		
	}

}
