package Week2_Day1;
 
public interface TvFeature {
	
	
	public void channelChange(int number);
	public void channelChange(String next);

	public void poweroff(boolean off);
	
	public void increasevolume(char sign);
	
	

}
