package corechallenge;

import java.util.Scanner;

public class CodeChallenge1 {

	public static void main(String[] args) {

		Scanner sc=new Scanner (System.in);
		System.out.println("Enter a table of the number");
		int table=sc.nextInt();
		System.out.println("Enter a nth number of the table");

		int nthnumber=sc.nextInt();

		System.out.println("Multiplication table of "+ table + " is :-");

		for(int i=1;i<=nthnumber;i++) {
			System.out.println(i + "*" + table + "=" + i*table);
		}

	}

}
