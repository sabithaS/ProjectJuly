package corechallenge;

import java.util.Scanner;

public class CodeChallenge2_LargestofThreeNumbers {

	public static void main(String[] args) {

		Scanner sc=new Scanner(System.in);

		System.out.println("Enter a 1st number");
		int number1=sc.nextInt();
		System.out.println("Enter a 2nd number");
		int number2=sc.nextInt();
		System.out.println("Enter a 3rd number");
		int number3=sc.nextInt();

		if(number1>number2&&number1>number3) {

			System.out.println("Number 1 is the greatest");
		}
		else if(number2>number1&&number2>number3) {
			System.out.println("Number 2 is the largest");
		}
		else {

			System.out.println("Number 3 is the largest");
		}


	}

}
