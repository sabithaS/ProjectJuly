package corechallenge;

import java.util.Scanner;

public class CodeChallenge2a_FindTheNumberOfGivenMonth {

	public static void main(String[] args) {

		Scanner sc=new Scanner(System.in);
		System.out.println("Enter a number of the month");
		int month=sc.nextInt();

		switch(month) {

		case 1:
			System.out.println("The " +month+ "st month of the year is January");
			System.out.println("Number of days in a " + month + "st month is 31" );
			break;
		case 2:
			System.out.println("The " +month+ " nd month of the year is February");
			System.out.println("Number of days in a" + month+ " nd month is 28" );
			break;
		case 3:
			System.out.println("The " +month+ " rd month of the year is March");
			System.out.println("Number of days in a" + month+ " rd month is 31" );
			break;
		case 4:
			System.out.println("The " +month+ " th month of the year is April");
			System.out.println("Number of days in a " + month + " th month is 30" );
			break;
		case 5:
			System.out.println("The " +month+ " th month of the year is May");
			System.out.println("Number of days in a " + month+ " th month is 31" );
			break;
		case 6:
			System.out.println("The " +month+ " th month of the year is June");
			System.out.println("Number of days in a" + month+ " th month is 30" );
			break;
		case 7:
			System.out.println("The " +month+ " th month of the year is July");
			System.out.println("Number of days in a" + month+" th month is 31" );
			break;
		case 8:
			System.out.println("The " +month+ " th month of the year is August");
			System.out.println("Number of days in a" + month+ "th month is 31" );
			break;
		case 9:
			System.out.println("The " +month+ " th month of the year is September");
			System.out.println("Number of days in a" + month+ "th month is 30" );
			break;
		case 10:
			System.out.println("The " +month+ " th month of the year is October");
			System.out.println("Number of days in a" + month+ "th month is 31" );
			break;
		case 11:
			System.out.println("The " +month+ "th month of the year is November");
			System.out.println("Number of days in a" + month+ " th month is 30" );
			break;
		case 12:
			System.out.println("The " +month+ " th month of the year is December");
			System.out.println("Number of days in a" + month+ "th month is 31" );
			break;
		default:
			System.out.println("There is no such month in a calender");



		}



	}

}
