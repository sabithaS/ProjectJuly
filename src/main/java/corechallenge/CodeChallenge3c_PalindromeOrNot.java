package corechallenge;

import java.util.Scanner;

public class CodeChallenge3c_PalindromeOrNot {

	public static void main(String[] args) {


		Scanner sc=new Scanner(System.in);

		System.out.println("Enter a number to check");

		int number=sc.nextInt();

		int s=0,r,t;

		while(number>0) {

			r=number%10;
			number=number/10;
			s=(s*10)+r;

		}if(number==s) {

			System.out.println("Given number is palindrome");
		}
		else {

			System.out.println("Given number is not a palindrome");
		}
	}}
