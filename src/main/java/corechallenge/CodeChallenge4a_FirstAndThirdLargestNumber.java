package corechallenge;


import java.util.Arrays;
import java.util.Scanner;

public class CodeChallenge4a_FirstAndThirdLargestNumber {

	public static void main(String[] args) {

		//Getting input from the user
		Scanner sc=new Scanner (System.in);

		System.out.println("Enter a number");

		int number=sc.nextInt();

		//Declaring an array elements

		int[] largest =new int[number];

		for(int i=0;i<largest.length;i++) {
			largest[i]=sc.nextInt();

		}
		Arrays.sort(largest);
		
		System.out.println("First largest number in array is "+ largest[number-2]);
		System.out.println("Third largest number in array is "+ largest[number-3]);





	}

}
