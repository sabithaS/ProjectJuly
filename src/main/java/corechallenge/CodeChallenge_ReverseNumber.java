package corechallenge;

import java.util.Scanner;

public class CodeChallenge_ReverseNumber {

	public static void main(String[] args) {

		Scanner sc=new Scanner(System.in);
		System.out.println("Enter a number");
		int number=sc.nextInt();
		int s=0,r;

		while(number>0) {

			r=number%10;
			number=number/10;
			s=s*10+r; 
		}
		System.out.println("Reverse value of " + number + " is " + s);




	}

}
