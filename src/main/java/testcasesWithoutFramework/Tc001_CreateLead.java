package testcasesWithoutFramework;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class Tc001_CreateLead {

	public static void main(String[] args) {
		//Setting property for chromedriver
		System.setProperty("webdriver.chrome.driver", "./Drivers/chromedriver.exe");

		ChromeDriver driver=new ChromeDriver();
		//To launch the url
		driver.get("http://leaftaps.com/opentaps/control/main");
		//To maximize the browser
		driver.manage().window().maximize();
		//To enter a values in login page
		driver.findElementById("username").sendKeys("DemoSalesManager");
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();
		driver.findElementByLinkText("CRM/SFA").click();
		//Navigate to create lead page
		driver.findElementByLinkText("Create Lead").click();
		//Create lead page

		driver.findElementById("createLeadForm_companyName").sendKeys("cognizant");
		driver.findElementById("createLeadForm_firstName").sendKeys("sabitha");

		//To select a value in dropdown:

		WebElement dropdownvalue = driver.findElementById("createLeadForm_dataSourceId");

		Select dropdown=new Select(dropdownvalue);

		dropdown.selectByVisibleText("Direct Mail");
		driver.findElementById("createLeadForm_firstNameLocal").sendKeys("chennai");
		driver.findElementById("createLeadForm_personalTitle").sendKeys("test");
		driver.findElementById("createLeadForm_generalProfTitle").sendKeys("testleaf");
		driver.findElementById("createLeadForm_annualRevenue").sendKeys("250000");
		//for a dropdown
		WebElement industry = driver.findElementById("createLeadForm_industryEnumId");
		Select industry1= new Select(industry);
		industry1.selectByValue("IND_SOFTWARE");
		//for a drop down

		WebElement ownership = driver.findElementById("createLeadForm_ownershipEnumId");
		Select ownership1=new Select(ownership);
		ownership1.selectByIndex(2);
		driver.findElementById("createLeadForm_sicCode").sendKeys("12345");
		driver.findElementById("createLeadForm_description").sendKeys("testleaf testleaf");
		driver.findElementById("createLeadForm_importantNote").sendKeys("major things");
		driver.findElementById("createLeadForm_lastName").sendKeys("srinivasan");
		driver.findElementById("createLeadForm_lastNameLocal").sendKeys("lastnamelocal");
		//for a dropdown
		WebElement marketing = driver.findElementById("createLeadForm_marketingCampaignId");
		Select marketing1=new Select(marketing);
		marketing1.selectByVisibleText("Car and Driver");
		driver.findElementById("createLeadForm_departmentName").sendKeys("commerce");

		//for a dropdown
		WebElement currency = driver.findElementById("createLeadForm_currencyUomId");
		Select currency1=new Select(currency);
		currency1.selectByValue("DZD");
		driver.findElementById("createLeadForm_numberEmployees").sendKeys("124");
		driver.findElementById("createLeadForm_tickerSymbol").sendKeys("$$$$");


		//contact information:
		driver.findElementById("createLeadForm_primaryPhoneCountryCode").sendKeys("2");
		driver.findElementById("createLeadForm_primaryPhoneAreaCode").sendKeys("600073");
		driver.findElementById("createLeadForm_primaryPhoneExtension").sendKeys("473025");
		driver.findElementById("createLeadForm_primaryEmail").sendKeys("test12345@yopmail.com");
		driver.findElementById("createLeadForm_primaryPhoneNumber").sendKeys("1234567890");
		driver.findElementById("createLeadForm_primaryPhoneAskForName").sendKeys("testleaf");
		driver.findElementById("createLeadForm_primaryWebUrl").sendKeys("https://www.google.com/");
		//Primary Address:
		driver.findElementById("createLeadForm_generalToName").sendKeys("muthukumari");
		driver.findElementById("createLeadForm_generalAddress1").sendKeys("no:12,test street,chennai");
		driver.findElementById("createLeadForm_generalCity").sendKeys("chennai");
		driver.findElementById("createLeadForm_generalPostalCode").sendKeys("600043");
		driver.findElementById("createLeadForm_generalPostalCodeExt").sendKeys("75023");
		driver.findElementById("createLeadForm_generalAttnName").sendKeys("testleaf");
		driver.findElementById("createLeadForm_generalAddress2").sendKeys("address2");
		//To dropdown to select a particular value
		WebElement country = driver.findElementById("createLeadForm_generalCountryGeoId");
		Select country1=new Select(country);
		country1.selectByVisibleText("India");
		//to dropdown

		WebElement state = driver.findElementByName("generalStateProvinceGeoId");
		Select state1=new Select(state);
		state1.selectByVisibleText("TAMILNADU");

		driver.findElementByName("submitButton").click();








	}

}
