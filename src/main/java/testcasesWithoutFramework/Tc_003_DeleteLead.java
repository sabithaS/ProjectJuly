package testcasesWithoutFramework;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Tc_003_DeleteLead {

	public static void main(String[] args) throws InterruptedException {



		System.setProperty("webdriver.chrome.driver", "./Drivers/chromedriver.exe");

		ChromeDriver driver=new ChromeDriver();
		//To launch the url
		driver.get("http://leaftaps.com/opentaps/control/main");
		//To maximize the browser
		driver.manage().window().maximize();
		//To enter a values in login page
		driver.findElementById("username").sendKeys("DemoSalesManager");
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByLinkText("Leads").click();
		driver.findElementByXPath("//a[text()='Find Leads']").click();
		driver.findElementByXPath(" //span[text()='Phone']").click();
		driver.findElementByName("phoneNumber").sendKeys("1234567890");
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		Thread.sleep(3000);
		
//to get a first lead id
		 String firstleadid = driver.findElementByXPath("(//a[@class='linktext'])[4]").getText();
		System.out.println("First lead id" + firstleadid);
//clicking on first resulting id
		
		driver.findElementByXPath("(//a[@class='linktext'])[4]").click();
		
		driver.findElementByLinkText("Delete").click();
		driver.findElementByXPath("//a[text()='Find Leads']").click();
		driver.findElementByName("id").sendKeys(firstleadid);
		//to click find leads button
		driver.findElementByXPath(" (//button[@class='x-btn-text'])[7]").click();
		Thread.sleep(3000);
		//to get a error message
		String errormessage = driver.findElementByXPath(" //div[@class='x-paging-info']").getText();
		System.out.println("Error " + errormessage);
		driver.quit();
	

		
		

	}

}
