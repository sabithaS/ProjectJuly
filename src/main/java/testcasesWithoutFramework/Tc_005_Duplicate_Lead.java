package testcasesWithoutFramework;

import org.openqa.selenium.chrome.ChromeDriver;

public class Tc_005_Duplicate_Lead {

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "./Drivers/chromedriver.exe");

		ChromeDriver driver=new ChromeDriver();
		//To launch the url
		driver.get("http://leaftaps.com/opentaps/control/main");
		//To maximize the browser
		driver.manage().window().maximize();
		//To enter a values in login page
		driver.findElementById("username").sendKeys("DemoSalesManager");
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByLinkText("Leads").click();
		driver.findElementByLinkText("Find Leads").click();
		//to click on email tab:
		driver.findElementByXPath("//span[text()='Email']").click();
		driver.findElementByName("emailAddress").sendKeys("abc@def.com");
		driver.findElementByXPath(" (//button[@class='x-btn-text'])[7]").click();
		Thread.sleep(3000);
		//capturing name of first resulting lead
		String firstname = driver.findElementByLinkText("Vijay").getText();
		System.out.println(firstname);

		driver.findElementByXPath("(//a[@class='linktext'])[4]").click();

		//clicking on duplicate lead
		driver.findElementByClassName("subMenuButton").click();
		String titledup = driver.getTitle();
		System.out.println(titledup);
		//creating a lead
		driver.findElementByClassName("smallSubmit").click();
		//getting the name
		String dupname = driver.findElementById("viewLead_firstName_sp").getText();
		System.out.println(dupname);

		if (firstname.equals(dupname)) {

			System.out.println("First name " + firstname + "and" + "Last name " + dupname + "are the same");
		}
		else {
			System.out.println("no name");
		}
		driver.quit();




	}

}
