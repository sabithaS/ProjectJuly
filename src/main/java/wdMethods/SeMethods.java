package wdMethods;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.NoSuchFrameException;
import org.openqa.selenium.NoSuchWindowException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.UnhandledAlertException;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import testNg.Extent_Report_Withrealtime_Scenario;

public class SeMethods  extends Extent_Report_Withrealtime_Scenario implements WdMethods{
	public int i = 1;
	public RemoteWebDriver driver;
	public void startApp(String browser, String url) {
		try {
			if(browser.equalsIgnoreCase("chrome")){
				System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
				ChromeOptions option = new ChromeOptions();
				option.addArguments("--disable-notifications");	
				driver = new ChromeDriver(option);
			} else if (browser.equalsIgnoreCase("firefox")){
				System.setProperty("webdriver.gecko.driver", "./drivers/geckodriver.exe");
				driver = new FirefoxDriver();
			}
			driver.get(url);
			driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			System.out.println("The Browser "+browser+" Launched Successfully");
		} catch (WebDriverException e) {
			System.err.println("The Browser "+browser+" not Launched");
		} finally {
			takeSnap();
		}

	}


	public WebElement locateElement(String locator, String locValue) {
		try {
			switch(locator) {
			case "id"	 : return driver.findElementById(locValue);
			case "class" : return driver.findElementByClassName(locValue);
			case "xpath" : return driver.findElementByXPath(locValue);
			case "link": return driver.findElementByLinkText(locValue);
			case "name": return driver.findElementByName(locValue); 
			}
		} catch (NoSuchElementException e) {
			System.err.println("The Element is not found");
		} catch (Exception e) {
			System.err.println("Unknow Exception ");
		} 
		return null;
	}

	@Override
	public WebElement locateElement(String locValue) {

		try {
			driver.findElementById(locValue);
			driver.findElementByClassName(locValue);
			driver.findElementByXPath(locValue);
			driver.findElementByLinkText(locValue);
			driver.findElementByName(locValue); 


		} catch (NoSuchElementException e) {
			System.out.println("No Element Found");
		}
		catch(Exception e) {
			System.err.println("Unknown exception");

		}
		return null;
	}


	public void type(WebElement ele, String data) {
		try {
			ele.sendKeys(data);
			//teststeps("The data "+data+ " is Entered Successfully", "pass");
			System.out.println("The data "+data+" is Entered Successfully");
		} catch (WebDriverException e) {
			//teststeps("The data "+data+ " is not Entered Successfully", "fail");
			System.out.println("The data "+data+" is Not Entered");
		} finally {
			takeSnap();
		}
	}


	public void clickWithNoSnap(WebElement ele) {
		try {
			ele.click();
			teststeps("The Element "+ele+" Clicked Successfully", "pass");
			System.out.println("The Element "+ele+" Clicked Successfully");
		} catch (Exception e) {
			teststeps("The Element "+ele+ " is not Clicked Successfully", "fail");
			System.err.println("The Element "+ele+"is not Clicked");
		}
	}


	@Override
	public void click(WebElement ele) {
		try {
			ele.click();
			teststeps("The Element "+ele+" Clicked Successfully", "pass");
			System.out.println("The Element "+ele+" Clicked Successfully");

		} catch (WebDriverException e) {
			teststeps("The Element "+ele+ " is not Clicked Successfully", "fail");

			System.err.println("The Element "+ele+"is not Clicked");
		} finally {
			takeSnap();
		}
	}
	public void clear(WebElement ele) {
		try {
			ele.clear();
			System.out.println("The Element "+ele+" Clicked Successfully");

		} catch (WebDriverException e) {
			System.err.println("The Element "+ele+"is not Clicked");
		} finally {
			takeSnap();
		}
	}






	@Override
	public String getText(WebElement ele) {
		String text = ele.getText();
		System.out.println(text+ " displayed successfully");


		return text;
	}

	@Override
	public void selectDropDownUsingText(WebElement ele, String value) {
		try {
			Select dd = new Select(ele);
			dd.selectByVisibleText(value);
			System.out.println("The DropDown Is Selected with VisibleText "+value);
		} catch (Exception e) {
			System.err.println("The DropDown Is not Selected with VisibleText "+value);
		} finally {
			takeSnap();
		}

	}

	@Override
	public void selectDropDownUsingIndex(WebElement ele, int index) {

		try {
			Select dd =new Select(ele);
			dd.selectByIndex(index);
			System.out.println("The DropDown Is Selected with index "+index);
		} catch (Exception e) {
			System.out.println("The DropDown Is not Selected with index "+index);
		}
		finally {
			takeSnap();
		}

	}





	@Override
	public void verifyPartialText(WebElement ele, String expectedText) {



		try {
			String text = ele.getText();
			boolean contains = text.contains(expectedText);
			System.out.println(expectedText);
			if(contains==true) {
				System.out.println("The given value " +  contains + " is displaying as expected text of " + expectedText );
			}
			else {
				System.out.println("The given value" + contains + "did not match with the expected text of " + expectedText);
			}
		} catch (Exception e) {

			System.out.println("something went wrong");
		}




	}

	@Override
	public void verifyExactAttribute(WebElement ele, String attribute, String value) {
		String attributevalue = ele.getAttribute(attribute);
		if(attributevalue.equalsIgnoreCase(value)) {
			System.out.println( value + "is displayed");

		}
		else {
			System.out.println( "value is not displayed properly");
		}


	}

	@Override
	public void verifyPartialAttribute(WebElement ele, String attribute, String value) {
		// TODO Auto-generated method stub

	}

	@Override
	public void verifySelected(WebElement ele) {


		boolean selected = ele.isSelected();

		if(selected==true) {
			System.out.println(ele + "selected successfully");

		}
		else {
			System.out.println(ele +"not selected");
		}



	}

	@Override
	public void verifyDisplayed(WebElement ele) {


		try {
			ele.isDisplayed();
			System.out.println("Element is displayed");
		} catch(NoSuchElementException e){
			System.out.println("No Such element present");

		}

		catch (Exception e) {
			System.out.println("unexpected error");
		}
		finally {
			takeSnap();
		}


	}





	@Override
	public void switchToFrame(WebElement ele) {

		try {
			driver.switchTo().frame(ele);
			System.out.println("Frame handled successfully");
		} catch (NoSuchFrameException e) {
			System.out.println("No Such frame");
		}
		finally {
			takeSnap();
		}


	}

	@Override
	public void acceptAlert() {

		try {
			driver.switchTo().alert().accept();
			System.out.println("Alert accepted successfully");
		} catch (UnhandledAlertException e) {
			System.out.println("Alert should be handled");
		}
		catch(NoAlertPresentException e) {
			System.out.println("No such alert");

		}

	}

	@Override
	public void dismissAlert() {
		try {
			driver.switchTo().alert().dismiss();
			System.out.println("�lert dismissed successfully");
		} catch (UnhandledAlertException e) {

			System.out.println("Alert should be handles");
		}
		catch(NoAlertPresentException e) {
			System.out.println("No such alert");


		}

	}

	@Override
	public String getAlertText() {
		String text = null;
		try {
			text = driver.switchTo().alert().getText();

			System.out.println("Alert" +text +"text found successfully");

		} catch (UnhandledAlertException e) {
			System.out.println("Alert should be handled");
		}
		catch(NoAlertPresentException e) {
			System.out.println("No Such Alert");

		}
		return text;
	}


	public void takeSnap() {
		File src = driver.getScreenshotAs(OutputType.FILE);
		File des = new File("./snaps/img"+i+".png");
		try {
			FileUtils.copyFile(src, des);
		} catch (IOException e) {
			System.err.println("IOException");
		}
		i++;
	}

	@Override
	public void closeBrowser() {
		driver.close();
		System.out.println("Current browser closed succesfully");

	}


	@Override
	public void closeAllBrowsers() {

		driver.quit();
		System.out.println("All browser closed succesfully");
	}


	@Override
	public void verifyExactText(WebElement ele, String expectedText) {

		try {
			String text = ele.getText();
			boolean contains = text.contains(expectedText);
			System.out.println(expectedText);
			if(contains==true) {
				System.out.println("The given value " +  contains + " is displaying as expected text of " + expectedText );
			}
			else {
				System.out.println("The given value " + contains + " did not match with the expected text of " + expectedText);
			}
		} catch (Exception e) {

			System.out.println("something went wrong");
		}



	}
	public void webdriverwait(WebElement ele) {


		WebDriverWait wait=new WebDriverWait(driver,20);
		wait.until(ExpectedConditions.elementToBeClickable(ele));




	}


	

	@Override
	public boolean verifyTitle(String expectedTitle) {

		try {
			String title = driver.getTitle();
			if(title.equals(expectedTitle)) {
				System.out.println( title + " Title entered successfully");


			}

			else {
				System.out.println("Title not found");
			}


		} catch (Exception e) {
			System.out.println("Something went wrong");		}
		return true;
	}




	@Override
	public void getWindowHandle(int index) {
		try {
			//Get window handles will return the set of window
			Set<String> newwindow = driver.getWindowHandles();
			System.out.println(newwindow.size());
			//To get the specific window index we are using list
			List<String> newwindow1=new ArrayList<String>();

			newwindow1.addAll(newwindow);
			String window1 = newwindow1.get(index);
			driver.switchTo().window(window1);
			driver.manage().window().maximize();
			System.out.println("System switched to new window");
		} catch (NoSuchWindowException e) {

			System.err.println("No Such Window");
		}
	}


	@Override
	public String getTitle(String title) {
		// TODO Auto-generated method stub
		return null;
	}









}

