package week1day1;

public class Currency_Reduction {

	public static void main(String[] args) {

		MobilePhone obj1= new MobilePhone();
		System.out.println("your mobile balance is INR 9");

		obj1.SMS("abcd", 9090897897l);
		System.out.println("Reminder-Your mobile balance is 8");

		obj1.Makeacall(9878678909l);	
		System.out.println("Reminder-Your mobile balance is 6");

		obj1.SMS("efgh", 9878678909l);
		System.out.println("Reminder-Your mobile balance is 5");	

		obj1.Makeacall(9867564678l);
		System.out.println("Reminder-Your mobile balance is 3");

		obj1.Makeacall(7867567898l);
		System.out.println("Reminder-Your mobile balance is 1");
		
		obj1.SMS("ijkl", 9878765678l);
		System.out.println("Reminder-Your mobile balance is 0");
		
		obj1.Makeacall(9878765678l);
		System.out.println("You can't make a call now because your balance is very low please recharge your mobile to get a service properly");



	}

}
