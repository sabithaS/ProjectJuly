package week1day1;

import java.util.Scanner;

public class MobileCompanyDetails {

	public static void main(String[] args) {

		Scanner sc=new Scanner(System.in);
		System.out.println("Print the mobile service code");

		int code=sc.nextInt();

		if(code==944) {

			System.out.println("your mobile service provider is BSNL because your mobile number starting with "+code);
		}

		else if(code==900) {
			System.out.println("your mobile service provider is AIRTEL because your mobile number starting with "+code);
		}
		else if(code==897) {
			System.out.println("your mobile service provider is idea because your mobile number starting with "+code);
		}
		else if(code==630) {

			System.out.println("your mobile service provider is JIO because your mobile number starting with "+code);
		}
		else {

			System.out.println("No mobile service provider for your given mobile number");
		}
	}

}
