package week1day1;

public class RelationalOperator {

	public static void main(String[] args) {
/*Relational Operator will return only the boolean value:
	== equal to
	!= not equal to
	< less than
	> greater than
	>= greater than or equal to*/

		//== equal to

		int a=90;

		int b=45;

		if(a==b) {

			System.out.println(true);
		}
		else {

			System.out.println(false);
		}

		//!= not equal to

		int c=50;
		int d=90;

		if(c!=d) {
			System.out.println(true);	
		}
		else {

			System.out.println(false);
		}

		//	<less than
		int e=90;
		int f=67;
		if(e<f) {
			System.out.println(true);
		}
		else {

			System.out.println(false);
		}
		//> greater than

		int g=45;
		int h=23;

		if(g>h) {
			System.out.println(true);
		}
		else {
			System.out.println(false);
		}
		//>= greater than or equal to
		int i=10;
		int j=15;
		if(i>=j) {
			System.out.println(true);
		}
		else {
			System.out.println(false);
		}





	}

}
