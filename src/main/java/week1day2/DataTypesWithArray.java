package week1day2;

import java.util.Scanner;

public class DataTypesWithArray {

	public static void main(String[] args) {

		//we have 8 data types;int,double,boolean.string,char,float,long,short,byte

		//int


		int intarray[]={1,2,3,4,5};
		System.out.println(intarray[0]);

		double doublearray[]= {10.5,10.9,10.7};
		System.out.println(doublearray[1]);

		boolean booleanarray[]= {true,false};
		System.out.println(booleanarray[1]);
		
		String stringarray[]= {"a","b"};
		System.out.println(stringarray[1]);
		
		char chararray[]= {'a','b','c'};
		System.out.println(chararray[0]);
		
		float floatarray[]= {10.60f,89.0f,67.5f};
		System.out.println(floatarray[2]);
		
		long longarray[]= {9600114367l,7890987657l,897659098l};
		System.out.println(longarray[0]);
		
		short shortarray[]= {305,450,560,450};
		System.out.println(shortarray[1]);
		
		byte bytearray[]= {120,119,1,2};
		System.out.println(bytearray[3]);
		
//Declaring an array
		int b=5;
		int a[] = new int[b];
		
	




	}

}


