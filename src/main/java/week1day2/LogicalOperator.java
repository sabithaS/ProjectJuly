package week1day2;

public class LogicalOperator {

	public static void main(String[] args) {

		//Logical operators are;

		/*&& And operator-Will return true when both the conditions are satisfied
		|| or operator-Will return true when one condition is satisfied.*/


		//&& AND operator

		int a =100;
		int b=200;
		int c=50;
		int d=100;
		if(a>b&&a>c&&a>d) {

			System.out.println("Greatest value is "+ a);
		}
		else {

			System.out.println("no value");
		}
		//|| or operator

		int g=10;
		int h=20;
		int i=30;
		int j=90;

		if(g>h||g>i||g>j) {

			System.out.println(true);
		}
		else {
			System.out.println(false);
		}
	}

}
