//to find whether the given checkbox is checked or not

package week3_codechallenge;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class Checkbox_Selection {

	public static void main(String[] args) throws InterruptedException {

		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver =new ChromeDriver();
		driver.get("http://www.leafground.com/pages/checkbox.html");
		//finding the checkbox xpath
		
		 WebElement checkbox = driver.findElementByXPath("//input[@type='checkbox']");
		 
		 Thread.sleep(3000);
		 //clicking the checkbox
		 checkbox.click();
		 //verifying whether is selected or not
		 if(checkbox.isSelected()) {
			 
			 System.out.println("Checkbox is selectted");
		 }
		 else {
			 System.out.println("Checkbox is not selected");
		 }
		 driver.close();
		 



	}

}
