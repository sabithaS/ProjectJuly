package week3_codechallenge;

import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class EditAndUpdateTheTitle {

	public static void main(String[] args) throws InterruptedException {

		System.setProperty("webdriver.chrome.driver", "./Drivers/chromedriver.exe");

		ChromeDriver driver=new ChromeDriver();
		//To launch the url
		driver.get("http://leaftaps.com/opentaps/control/main");
		//To maximize the browser
		driver.manage().window().maximize();
		//To enter a values in login page
		driver.findElementById("username").sendKeys("DemoSalesManager");
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByXPath("//a[text()='Leads']").click();

		driver.findElementByXPath("//a[text()='Find Leads']").click();
		driver.findElementByXPath("(//input[@name='firstName'])[3]").sendKeys("sabitha");
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		/*webDriverWait wait=new WebDriverWait(driver, 10);
		driver.findElementByXPath("( //a[@class='linktext'])[4]").click();*/

		WebDriverWait wait=new WebDriverWait(driver,10);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//a[@class='linktext'])[4]")));
		driver.findElementByXPath("(//a[@class='linktext'])[4]").click();

		String title1 = driver.getTitle();


		System.out.println(title1);
		Thread.sleep(3000);
		driver.findElementByLinkText("Edit").click();
		Thread.sleep(3000);
		driver.findElementById("updateLeadForm_companyName").clear();
		driver.findElementById("updateLeadForm_companyName").sendKeys("cognizant");

		driver.findElementByClassName("smallSubmit").click();
		String comapny = driver.findElementById("viewLead_companyName_sp").getText();
		System.out.println(comapny);



	}

}
