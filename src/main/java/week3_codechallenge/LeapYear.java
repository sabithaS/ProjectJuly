package week3_codechallenge;

import java.util.Scanner;

public class LeapYear {

	public static void main(String[] args) {

		Scanner sc=new Scanner(System.in);
		int year=sc.nextInt();
		//leapyear is=1)If its divisible by 4 nad 400 and if the number is not divisible by 100 is leap year

		if(year%4==0&& year%100!=0|| year%400==0) {

			System.out.println("The entered " + year + "is a leap year");
		}
		else {
			System.out.println("The entered " + year + "is not a leap year");
		}


	}

}
