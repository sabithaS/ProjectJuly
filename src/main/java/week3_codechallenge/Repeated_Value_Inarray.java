package week3_codechallenge;

import java.util.Scanner;

public class Repeated_Value_Inarray {



	public static void main(String[] args) {

		/*Checking the password rules.Below are the conditions

		Password rules:A password must have atleast ten character c;
	A password consists of only letters and digits
	A password must contain atleast two digits and two numbers
	A password must contain atleast one capital letter
		 */

		Scanner sc=new Scanner(System.in);
		System.out.println("Enter a Password");
		//Declaring string because string will contain letters and digits
		String Password1=sc.nextLine();
		if(isValid(Password1)) {

			System.out.println("Valid Password"); 
		}
		else {

			System.out.println("Invalid Password");
		}
	}

	private static boolean isValid(String Password) {
		//Declaring and initiating the variables	
		char c;
		int countdigits=0,countletter=0,countuppercase=0;
		if(Password.length()<10) {
			System.out.println("Password should have only ten character");

			return false;

		}

		else {

			for(int i=0;i<Password.length()-1;i++) {
				c=Password.charAt(i);
				if(!Character.isLetterOrDigit(c)) {

					System.out.println("Password should not have special character");
					return false;
				}
				if(Character.isDigit(c)) {

					countdigits++;
				}
				else if(Character.isLetter(c)) {

					countletter++;


				}

			}


		}

		if(countdigits<2) {
			System.out.println("Password must contain atleast 2 digits");
			return false;

		}
		else if(countletter<2) {
			System.out.println("Password must contain atleast 2 letters");
			return false;
		}
		else if(countuppercase<1) {
			System.out.println("Password must contain atleast 1 capital letter");
			return false;


		}

		return true;

	}

}
