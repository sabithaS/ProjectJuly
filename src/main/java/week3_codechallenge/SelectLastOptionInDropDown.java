package week3_codechallenge;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class SelectLastOptionInDropDown {

	public static void main(String[] args) {


		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		driver.get("http://www.leafground.com/pages/Dropdown.html");
		driver.manage().window().maximize();
		//identified the dropdown locator
		WebElement dropdown = driver.findElementById("dropdown1");
		//wrote a class for a dropdown

		Select dropdown1=new Select(dropdown);

		//why we are using list to get all the options from dropdown
		List<WebElement> alloption = dropdown1.getOptions();
		System.out.println(alloption);

		int lastoption = alloption.size()-1;
		int i=1;
		for (WebElement eachoption : alloption) {


			if(i==lastoption) {
				eachoption.click();


			}
			i++;

		}



	}}


