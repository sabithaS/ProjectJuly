package week3_codechallenge;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class SelectingAllValueFromDropDown {

	public static void main(String[] args) throws InterruptedException {

		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		driver.get("https://www.irctc.co.in/eticketing/userSignUp.jsf");
		driver.manage().window().maximize();
		Thread.sleep(3000);
		//to find the locator of the dropdown
		WebElement country = driver.findElementById("userRegistrationForm:countries");
		Select alloption=new Select(country);
		List<WebElement> options = alloption.getOptions();
		System.out.println(options);
		for (WebElement eachoption : options) {

			System.out.println(eachoption.getText());

		}





	}

}
