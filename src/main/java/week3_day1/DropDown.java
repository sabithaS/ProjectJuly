package week3_day1;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class DropDown {

	public static void main(String[] args) {

		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");

		ChromeDriver driver= new ChromeDriver();
		driver.get("http://leaftaps.com/opentaps/control/main");
		driver.findElementById("username").sendKeys("DemoSalesManager");
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByLinkText("Create Lead").click();
		driver.findElementById("createLeadForm_companyName").sendKeys("cognizant");
		driver.findElementById("createLeadForm_firstName").sendKeys("sabitha");
		driver.findElementById("createLeadForm_lastName").sendKeys("srinivasan");

		WebElement dropdown = driver.findElementById("createLeadForm_dataSourceId");

		Select options = new Select(dropdown);

		options.selectByVisibleText("Conference");

		//to print all values from dropdown
		WebElement marketValues = driver.findElementById("createLeadForm_marketingCampaignId");
		Select dd = new Select(marketValues);

		List<WebElement> allValues = dd.getOptions();
		dd.selectByValue("CATRQ_CARNDRIVER");
		for (WebElement eachValue : allValues) {
			System.out.println(eachValue.getText());
		}


	}

}
