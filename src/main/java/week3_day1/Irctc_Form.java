package week3_day1;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class Irctc_Form {

	public static void main(String[] args) throws InterruptedException {

		System.setProperty("webdriver.chrome.driver", "./Drivers/chromedriver.exe");

		ChromeDriver driver=new ChromeDriver();

		driver.get("https://www.irctc.co.in/nget/train-search");
		driver.manage().window().maximize();

		driver.findElementByXPath("(//span[@class='ng-star-inserted'])[1]").click();

		driver.findElementByLinkText("Sign up").click();

		//driver.get("https://www.irctc.co.in/eticketing/userSignUp.jsf");
		driver.findElementById("userRegistrationForm:userName").sendKeys("579360");
		driver.findElementById("userRegistrationForm:password").sendKeys("Sabi()1996");
		driver.findElementById("userRegistrationForm:confpasword").sendKeys("Sabi()1996");
		//For dropdown
		WebElement secque = driver.findElementById("userRegistrationForm:securityQ");

		Select secque1=new Select(secque);
		secque1.selectByVisibleText("Who was your Childhood hero?");
		driver.findElementById("userRegistrationForm:securityAnswer").sendKeys("father");
		//For language dropdown
		WebElement language = driver.findElementById("userRegistrationForm:prelan");
		Select lan1=new Select(language);

		lan1.selectByValue("en");

		//Personal details
		driver.findElementById("userRegistrationForm:firstName").sendKeys("sabtiha");
		driver.findElementById("userRegistrationForm:middleName").sendKeys("NA");
		driver.findElementById("userRegistrationForm:lastName").sendKeys("srinivasan");
		driver.findElementById("userRegistrationForm:gender:1").click();
		driver.findElementById("userRegistrationForm:maritalStatus:1").click();
		//for DOB dropdown
		WebElement date = driver.findElementById("userRegistrationForm:dobDay");
		Select date1=new Select(date);
		date1.selectByVisibleText("18");
		//for month dropdown

		WebElement month = driver.findElementById("userRegistrationForm:dobMonth");
		Select month1=new Select(month);
		month1.selectByVisibleText("FEB");
		//for year dropdown
		WebElement year = driver.findElementById("userRegistrationForm:dateOfBirth");
		Select year1=new Select(year);
		year1.selectByVisibleText("1996");
		//for occupation dropdown
		WebElement occupation = driver.findElementById("userRegistrationForm:occupation");
		Select occu=new Select(occupation);
		occu.selectByVisibleText("Private");
		driver.findElementById("userRegistrationForm:uidno").sendKeys("1234567890");
		driver.findElementById("userRegistrationForm:idno").sendKeys("PAN123456");
		//country dropdown

		WebElement country = driver.findElementById("userRegistrationForm:countries");
		Select country1=new Select(country);
		country1.selectByValue("94");

		driver.findElementById("userRegistrationForm:email").sendKeys("test12345@yopmail.com");
		driver.findElementById("userRegistrationForm:mobile").sendKeys("9089898765");

		//For nationality dropdown
		WebElement nationality = driver.findElementById("userRegistrationForm:nationalityId");
		Select nationality1=new Select(nationality);
		nationality1.selectByValue("94");	

		//Resedential address

		driver.findElementById("userRegistrationForm:address").sendKeys("12");
		driver.findElementById("userRegistrationForm:street").sendKeys("street");
		driver.findElementById("userRegistrationForm:area").sendKeys("chennai");
		driver.findElementById("userRegistrationForm:pincode").sendKeys("600073",Keys.TAB);
		Thread.sleep(3000);
		WebElement city = driver.findElementById("userRegistrationForm:cityName");
		Select city1=new Select(city);
		city1.selectByValue("Kanchipuram");
		Thread.sleep(3000);
		WebElement postoffice = driver.findElementById("userRegistrationForm:postofficeName");
		Select postoffice1=new Select(postoffice);
		postoffice1.selectByVisibleText("Selaiyur S.O");

		driver.findElementById("userRegistrationForm:landline").sendKeys("9089876545");
	}

}
