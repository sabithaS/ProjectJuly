package week3_day1;

import java.util.NoSuchElementException;

import org.openqa.selenium.chrome.ChromeDriver;

public class TryCatchWithSelenium {

	public static void main(String[] args) {

		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");

		ChromeDriver driver=new ChromeDriver();

		driver.get("http://leaftaps.com/opentaps/");
		try {
			driver.findElementById("username1").sendKeys("DemoSalesManager");
		} catch (NoSuchElementException e) {
			System.out.println("invalid input");
		}

		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();

	}

}
