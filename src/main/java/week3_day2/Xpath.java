package week3_day2;

import org.openqa.selenium.chrome.ChromeDriver;

public class Xpath {

	public static void main(String[] args) {


		System.setProperty("webdriver.chrome.driver", "./Drivers/chromedriver.exe");

		ChromeDriver driver=new ChromeDriver();
		//To launch the url
		driver.get("http://leaftaps.com/opentaps/control/main");
		//To maximize the browser
		driver.manage().window().maximize();
		//To enter a values in login page
		driver.findElementById("username").sendKeys("DemoSalesManager");
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByLinkText("Leads").click();

		driver.findElementByLinkText("Merge Leads");
	//	driver.findElementById("ComboBox_partyIdFrom").sendKeys("10501");
		driver.findElementByXPath(" (//img[@alt='Lookup'])[1]").click();
		


	}

}
