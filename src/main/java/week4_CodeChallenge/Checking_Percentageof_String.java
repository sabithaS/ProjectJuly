package week4_CodeChallenge;

import java.util.Scanner;

public class Checking_Percentageof_String {

	public static void main(String[] args) {

		/*We are going to check the percentage for the following fields
1)Percentage of character
2)percentage of lowercase
3)Percentage of uppercase
4)Percentage of digit
5)percentage of other special character(including space)*/

		Scanner sc=new Scanner(System.in);

		System.out.println("Enter a name");
		String name=sc.nextLine();

		int length = name.length();

		char c;
		int lowercase,uppercase,digit,specialcharacter;
		lowercase=uppercase=digit=specialcharacter=0;

		for(int i=0;i< length;i++) {
			c=name.charAt(i);
			if(Character.isDigit(c)) {
				digit++;


			}
			else if(Character.isLowerCase(c)) {
				lowercase++;
			}
			else if(Character.isUpperCase(c)) {
				uppercase++;
			}
			else {
				specialcharacter++;

			}
		}

		//Declaring a float to get a percentage
		float f1,f2,f3,f4;

		f1=(uppercase*100/length);
		f2=(lowercase*100/length);
		f3=(digit*100/length);
		f4=(specialcharacter*100/length);
		System.out.println("Number of upper case letter is " + uppercase + " and percentage is " + f1+ "%");
		System.out.println("Number of lower case letter is " + lowercase + " and percentage is " + f2+ "%");
		System.out.println("Number of digit is " + digit + " and percentage is" + f3+ "%");
		System.out.println("Number of special character is " + specialcharacter + " and percentage is " + f4+ "%");




	}

}
