package week4_CodeChallenge;

import java.util.Scanner;

public class LargestNumber_Lessthangivendigit {

	public static void main(String[] args) {

		//We need to find the largest number less than the given number
		//it should not contain the given digit


		Scanner sc=new Scanner(System.in);
		System.out.println("Enter a number");
		int number=sc.nextInt();
		System.out.println("Enter a digit form 1-9");
		int digit=sc.nextInt();

		number--;

		while(Integer.toString(number).contains(Integer.toString(digit))) {

			number--;
		}
		System.out.println("largest number " +number);





	}

}
