package week4_CodeChallenge;

import java.util.Scanner;

public class Multiple_Of_3or5 {

	public static void main(String[] args) {

	
	Scanner sc=new Scanner(System.in);
	System.out.println("Enter a number");
	
	int number=sc.nextInt();
	
	//Finding the multiple of 3 or 5 in the given number
	
	int sum=0;
	
	for(int i=3;i<number;i++) {
		
		if(i%3==0||i%5==0) {
			
			sum =sum+ i;
}
	}

	System.out.println("sum of multiple of 3 or 5 less than the given number " + number+ "is " + sum);
	
	
	
	}

}
