package week4_CodeChallenge;

import java.util.Scanner;

public class String_Passwordverification {





	public static void main(String[] args) {

		/*Checking the password rules.Below are the conditions

		Password rules:A password must have atleast ten character c;
	A password consists of only letters and digits
	A password must contain atleast two digits and two numbers
	A password must contain atleast one capital letter
		 */

		Scanner sc=new Scanner(System.in);                                                                                                                                                                                                                                                                                                                               
		System.out.println("Enter a Password");
		//Declaring string because string will contain letters and digits
		String Password1=sc.nextLine();
		//Is valid is a method is declared outside of the main method with the below conditions
		if(isValid(Password1)) {

			System.out.println("Valid Password"); 
		}

		else {

			System.out.println("Invalid Password");
		}
	}

	private static boolean isValid(String Password) {
		//Declaring and initiating the variables	
		char c;
		int countdigits=0,countletter=0,countuppercase=0;
		//Checking the password length
		if(Password.length()<10) {
			System.out.println("Password should have only ten character");

			return false;

		}
		//if the below conditions are satisfied password will became valid
		else {

			for(int i=0;i<Password.length()-1;i++) {
				c=Password.charAt(i);
				//if character is not equal to letter or digit
				if(!Character.isLetterOrDigit(c)) {

					System.out.println("Password should not have special character");
					return false;
				}
				//Checking the character is digit will increment the digit value
				if(Character.isDigit(c)) {

					countdigits++;
				}
				//checking the letter value if it is letter it will increment the letter
				else if(Character.isLetter(c)) {

					countletter++;


				}

			}


		}
		/*//checking the below conditions
		digit should not exceed more than 2
		letter should not exceed more than 2
		uppercase should not exceed more than 1*/

		if(countdigits<2) {
			System.out.println("Password must contain atleast 2 digits");
			return false;

		}
		else if(countletter<2) {
			System.out.println("Password must contain atleast 2 letters");
			return false;
		}
		else if(countuppercase<1) {
			System.out.println("Password must contain atleast 1 capital letter");
			return false;


		}
		//if the above conditions are satisfied it will return true
		return true;

	}


}



