package week4_day1;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class AlertAndFrame {

	public static void main(String[] args) {

		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");

		ChromeDriver driver=new ChromeDriver();

		driver.get("https://www.w3schools.com/js/tryit.asp?filename=tryjs_prompt");
		driver.manage().window().maximize();
		//using id\
		// driver.switchTo().frame("iframeResult");
		//using xpath(Web element)	
		driver.switchTo().frame(driver.findElementByXPath("//iframe[@name='iframeResult']"));
		driver.findElementByXPath("//button[text()='Try it']").click();
		//Switching to an alert
		driver.switchTo().alert().sendKeys("sabitha");
		//accepting an alert
		driver.switchTo().alert().accept();
		//getting text which is updated through alert
		String text = driver.findElementById("demo").getText();
		System.out.println(text);
		//to come out of frame
		driver.switchTo().defaultContent();
		//clicking outside of the frame
		driver.findElementById("tryhome").click();





	}

}
