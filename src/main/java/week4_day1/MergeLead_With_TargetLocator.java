package week4_day1;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class MergeLead_With_TargetLocator {

	public static void main(String[] args) throws InterruptedException {
		int i=1;


		System.setProperty("webdriver.chrome.driver", "./Drivers/chromedriver.exe");

		ChromeDriver driver=new ChromeDriver();
		//To launch the url
		driver.get("http://leaftaps.com/opentaps/control/main");
		//To maximize the browser
		driver.manage().window().maximize();
		//To enter a values in login page
		driver.findElementById("username").sendKeys("DemoSalesManager");
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByLinkText("Leads").click();

		driver.findElementByLinkText("Merge Leads").click();

		driver.findElementByXPath(" (//img[@alt='Lookup'])[1]").click();
		//storing primary window
		String storingprimary = driver.getWindowHandle();

		//creating new window
		Set<String> merge = driver.getWindowHandles();
		System.out.println(merge.size());
		List<String> merge1=new ArrayList<String>();
		merge1.addAll(merge);
		String newwindow = merge1.get(1);
		driver.switchTo().window(newwindow);
		driver.manage().window().maximize();
		driver.findElementByName("id").sendKeys("10103");
		driver.findElementByClassName("x-btn-text").click();
		Thread.sleep(3000);
		driver.findElementByXPath("(//a[@class='linktext'])[1]").click();

		//switching back to primary window calling the primary window string
		driver.switchTo().window(storingprimary);

		driver.findElementByXPath(" (//img[@alt='Lookup'])[2]").click();

		//switching back to new window

		Set<String> newwindow1 = driver.getWindowHandles();
		System.out.println(newwindow1.size());
		List<String> obj1=new ArrayList<String>();
		obj1.addAll(newwindow1);
		String secondwin = obj1.get(1);
		driver.switchTo().window(secondwin);
		driver.manage().window().maximize();

		//processing the new window:
		driver.findElementByName("id").sendKeys("10104");
		driver.findElementByClassName("x-btn-text").click();
		Thread.sleep(3000);
		driver.findElementByXPath("(//a[@class='linktext'])[1]").click();
		//again switch back to primarywindow
		driver.switchTo().window(storingprimary);
		driver.findElementByLinkText("Merge").click();
		//clicking ok on alert
		driver.switchTo().alert().accept();
		//clicking find leads
		driver.findElementByLinkText("Find Leads").click();
		//entering lead id
		driver.findElementByName("id").sendKeys("10103");

		driver.findElementByXPath(" (//button[@class='x-btn-text'])[7]").click();
		Thread.sleep(3000);
		System.out.println(merge1.size());

		//to get a error message

		String errormessage = driver.findElementByXPath(" //div[@class='x-paging-info']").getText();
		System.out.println("Error " + errormessage);

		//to get a snap shot:
		File src = driver.getScreenshotAs(OutputType.FILE);
		File dest= new File("./snaps/img.png");
		try {
			FileUtils.copyFile(src, dest);
		} catch (IOException e) {
			System.out.println("error on taking screenshots");
		}


		driver.quit();
























	}

}
