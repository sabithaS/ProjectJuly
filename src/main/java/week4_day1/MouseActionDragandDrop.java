package week4_day1;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class MouseActionDragandDrop {

	public static void main(String[] args) throws InterruptedException {

		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");

		ChromeDriver driver=new ChromeDriver();

		driver.get("http://jqueryui.com/draggable/");
		driver.manage().window().maximize();
		Thread.sleep(3000);
	
driver.switchTo().frame(0);
WebElement drag = driver.findElementById("draggable");
Actions builder=new Actions(driver);
builder.dragAndDropBy(drag, 100, 100).perform();

		



	}

}
