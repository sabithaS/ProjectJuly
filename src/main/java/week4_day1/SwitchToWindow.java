package week4_day1;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class SwitchToWindow {

	public static void main(String[] args) throws InterruptedException {

		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");

		ChromeDriver driver=new ChromeDriver();

		driver.get("https://www.irctc.co.in/eticketing/loginHome.jsf");
		driver.manage().window().maximize();
		Thread.sleep(3000);

		driver.findElementByXPath("(//span[text()='AGENT LOGIN'])[1]").click();

		//click link
		driver.findElementByLinkText("Contact Us").click();		
		Set<String> allWindows = driver.getWindowHandles();
		System.out.println(allWindows.size());

		List<String> newWindow = new ArrayList<String>();
		newWindow.addAll(allWindows);
		String WinName = newWindow.get(1);
		driver.switchTo().window(WinName);
		String title = driver.getTitle();
		System.out.println(title);
		String currentUrl = driver.getCurrentUrl();
		System.out.println(currentUrl);
		//driver.quit();



	}

}
