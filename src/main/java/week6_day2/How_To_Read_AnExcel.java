package week6_day2;

import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class How_To_Read_AnExcel {

	public static void main(String[] args) throws IOException {
		
//locate work book using the XSSworkbook class.It will contain all the methods related to work book
		XSSFWorkbook wbook = new XSSFWorkbook("./data/CreateLead.xlsx");
		//from workbook we are moving to sheet
		XSSFSheet sheet = wbook.getSheet("createlead");
		//Getting the value of number of row
		int lastRowNum = sheet.getLastRowNum();
		//getting the row details
		//Excel row will start from 1 because 0 is a header
		XSSFRow row = sheet.getRow(1);
	
	//Getting the last column number
		int lastCellNum = row.getLastCellNum();
		//Getting the first cell details
		XSSFCell cell = row.getCell(0);
		//Getting the value form the excel
		String stringCellValue = cell.getStringCellValue();
		//It will print the first row and first column details
		System.out.println(stringCellValue);
		
		
		
		
		
		

	
	
	
	
	}

}
