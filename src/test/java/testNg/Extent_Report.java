package testNg;

import java.io.IOException;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.MediaEntityModelProvider;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class Extent_Report {

	public static void main(String[] args) throws IOException {


		//To create a html file
		ExtentHtmlReporter html=new ExtentHtmlReporter("./report/result.html");
		//to create an history of test cases
		html.setAppendExisting(true);
		//To make the html report in editable mode
		ExtentReports extent =new ExtentReports();
		extent.attachReporter(html);		
		//To create a field to enter a test case
		ExtentTest test = extent.createTest("TC_001", "Create Lead");
		//calling an variable to enter a test cases
		test.pass("The data demo sales manager successfully ",
				//There will be a chances for no image in path so we are throwing an exception
				//../ will extent the child to parent
				MediaEntityBuilder.createScreenCaptureFromPath("./../Snaps/img.png").build());

		test.pass("The data crmsfa entered successfully",MediaEntityBuilder.createScreenCaptureFromPath("./../Snaps/img.png").build());
		test.fail("log in button is not clicked");
		//Without this the html code itself will not create
		extent.flush();








	}

}
