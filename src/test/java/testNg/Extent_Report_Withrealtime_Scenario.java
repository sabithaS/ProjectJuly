package testNg;

import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class Extent_Report_Withrealtime_Scenario {

	public static  ExtentReports extent;
	public static ExtentTest test;
	public String testCasename,testCaseDescription,author,category;


	@BeforeSuite(groups="common")//one time execution before starting the test case execution
	public void htmlCreation() {

		//To create a html file
		ExtentHtmlReporter html=new ExtentHtmlReporter("./report/result.html");
		//to create an history of test cases
		html.setAppendExisting(true);
		//To make the html report in editable mode
		extent =new ExtentReports();
		extent.attachReporter(html);	
	}
	@BeforeMethod(groups="common")
	public  void testCases() {
		test = extent.createTest(testCasename, testCaseDescription);
		test.assignAuthor(author);
		test.assignCategory(category);


	}


	public void teststeps(String desc,String status) {
		if(status.equalsIgnoreCase("pass")) {
			test.pass(desc);
		}
		if(status.equalsIgnoreCase("fail")) {
			test.fail(desc);

		}
	}
	//It will run after execution of suite
	@AfterSuite(groups="common")
	public void stopResult() {

		extent.flush();
	}









}
