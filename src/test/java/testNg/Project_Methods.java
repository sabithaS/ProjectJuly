package testNg;

import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;

import wdMethods.SeMethods;

public class Project_Methods extends SeMethods{

	@BeforeMethod(groups="common")
	
	@Parameters({"browser","url","username","password"})
	
	public void login (String browser,String url, String UNam, String Pass) {

		startApp(browser, url);

		WebElement username = locateElement("id", "username");	
		//to send the value for a element 
		type(username, UNam);
		WebElement password = locateElement("id", "password");
		type(password, Pass);
		//To click the login button
		WebElement login = locateElement("class", "decorativeSubmit");
		click(login);
		//to click the crmsfa link
		WebElement crmsfa = locateElement("link", "CRM/SFA");
		click(crmsfa);
	}
	
	@AfterMethod(groups="common")
	public void logout() {
		closeBrowser();
	}
}
