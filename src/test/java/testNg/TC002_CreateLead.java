package testNg;


import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class TC002_CreateLead extends Project_Methods {

	@BeforeClass(groups="common")

	public void testdata() {

		/*test.assignAuthor(author);
		test.assignCategory(category);*/
		testCasename="TC001";
		testCaseDescription="Create a lead";
	}

	@Test(invocationCount=2,invocationTimeOut=60000 ,groups="smoke",dataProvider="Create lead")


	public void createlead(String cName,String fName,String lName,String eMail,String pNum){

		/*To launch the chromebrowser
1)It will launcht the browser load the url
2)maximize the window and will take the snap*/
		//Calling login method	
		//to click the create lead
		WebElement createlead = locateElement("link", "Create Lead");
		click(createlead);
		//to enter a company name

		WebElement company = locateElement("id", "createLeadForm_companyName");
		type(company, cName);
		//to enter a first name
		WebElement firstname = locateElement("id", "createLeadForm_firstName");
		type(firstname, fName);
		//to enter a last name
		WebElement lastname = locateElement("id", "createLeadForm_lastName");
		type(lastname, lName);
		//to give a value in a source dropdown
		WebElement sourcedd = locateElement("id", "createLeadForm_dataSourceId");
		selectDropDownUsingText(sourcedd, "Conference");
		//to give a value in marketcampaign dropdown
		WebElement marketcampaign = locateElement("id", "createLeadForm_marketingCampaignId");
		selectDropDownUsingIndex(marketcampaign, 2);

		//to enter a email address
		WebElement email = locateElement("id", "createLeadForm_primaryEmail");
		type(email, eMail);
		//to enter a phone number
		WebElement phonenumber = locateElement("id", "createLeadForm_primaryPhoneNumber");
		type(phonenumber, pNum);
		WebElement createleadbutton = locateElement("class", "smallSubmit");
		click(createleadbutton);
		WebElement firstnameverify = locateElement("id", "viewLead_firstName_sp");
		verifyExactText(firstnameverify, "sabitha");
		//closeBrowser();

	}


	@DataProvider(name="Create lead")
	public Object[][] datas() {

		//Cretaing two dimentional array to enter a multiple data for single TC


		Object[][] data =new Object[2][5];
		//First set of data 
		data[0][0]="cts";
		data[0][1]="sabitha";
		data[0][2]="s";
		data[0][3]="sab@g.com";
		data[0][4]="1234567890";
		
		//Second set of data
		data[1][0]="amazon";
		data[1][1]="kavitha";
		data[1][2]="s";
		data[1][3]="kav@g.com";
		data[1][4]="1234567890";


		return data;



	}




}
