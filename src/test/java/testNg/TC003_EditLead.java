package testNg;

import org.testng.annotations.Test;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;

import wdMethods.SeMethods;

public class TC003_EditLead extends Project_Methods {
	@BeforeClass

	public void testdata() {


		author="sabitha";
		category="Smoke";

		testCasename="TC002";
		testCaseDescription="Edit a lead";


	}


	@Test(/*dependsOnMethods="testNg.TC002_CreateLead.createlead"*/groups="Sanity"/*,dependsOnGroups="smoke"*/)

	public void editLead() {
		/*//to login to application below methods are called
		//to start up a chrome browser.
		startApp("chrome", "http://leaftaps.com/opentaps");
		//to enter a username
		WebElement username = locateElement("id", "username");	
		type(username, "demosalesmanager");
		//to enter a password
		WebElement password = locateElement("id", "password");	
		type(password, "crmsfa");
		//to click the login button
		WebElement login = locateElement("class", "decorativeSubmit");
		click(login);
		//to click the crmsfa link
		WebElement link = locateElement("link", "CRM/SFA");
		click(link);*/
		//login();
		//to click the leads
		WebElement leads = locateElement("link", "Leads");
		click(leads);
		//to click the lead
		WebElement findleads = locateElement("link", "Find Leads");
		click(findleads);
		//enter a first name
		/*WebElement firstname = locateElement("name", "firstName");
		type(firstname, "sabitha");*/
		WebElement name = locateElement("xpath", "(//input[@name='firstName'])[3]");
		type(name, "sabitha");


		//click find leads button with webdriver wait
		WebElement findleadsbutton = locateElement("xpath", "//button[text()='Find Leads']");
		click(findleadsbutton);
		webdriverwait(findleadsbutton);
		WebElement firstlead = locateElement("xpath", "(//a[@class='linktext'])[4]");
		click(firstlead);
		//Verify title of the page
		String title = getTitle("View Lead | opentaps CRM");
		verifyTitle("View Lead | opentaps CRM");

		//to click the edit button
		WebElement editlink = locateElement("link", "Edit");
		click(editlink);
		//To clear the company name
		WebElement cleartext = locateElement("id", "updateLeadForm_companyName");
		clear(cleartext);
		//To modify the company name
		WebElement modifycompany = locateElement("id", "updateLeadForm_companyName");
		type(modifycompany, "Amazon");
		//For clicking update button
		WebElement clickingupdate = locateElement("class", "smallSubmit");
		click(clickingupdate);
		WebElement changedname = locateElement("id", "viewLead_companyName_sp");
		verifyExactText(changedname, "Amazon");
		//To close the browser
		closeBrowser();















	}


}





