package testNg;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.openqa.selenium.WebElement;

import wdMethods.SeMethods;


public class TC004_Delete_Lead extends Project_Methods  {

	@BeforeClass(groups="common")

	public void testdata() {

		author="sabitha";
		category="smoke";
		testCasename="TC003";
		testCaseDescription="Delete lead";


	}
	@Test(/*dependsOnMethods="testNg.TC003_EditLead.editLead"*//*groups="Regression",dependsOnGroups="Sanity"*/)
	public void deletingLead () {

		/*startApp("chrome", "http://leaftaps.com/opentaps/control/main");

		WebElement username = locateElement("id", "username");	
		//to send the value for a element 
		type(username, "demosalesmanager");
		WebElement password = locateElement("id", "password");
		type(password, "crmsfa");
		//To click the login button
		WebElement login = locateElement("class", "decorativeSubmit");
		click(login);
		//to click the crmsfa link
		WebElement crmsfa = locateElement("link", "CRM/SFA");
		click(crmsfa);*/
		//login();
		//to click the leads
		WebElement leads = locateElement("link", "Leads");
		click(leads);
		//to click the find lead link
		WebElement findleads = locateElement("link", "Find Leads");
		click(findleads);
		//to click on phone field
		WebElement phonefield = locateElement("xpath", " //span[text()='Phone']");
		click(phonefield);
		//To enter a phone number
		WebElement phonenumber = locateElement("name", "phoneNumber");
		type(phonenumber, "1234567890");
		//To click on Find leads button
		WebElement findleadsbutton = locateElement("xpath", "//button[text()='Find Leads']");
		click(findleadsbutton);
		webdriverwait(findleadsbutton);
		//TO get the first lead id
		WebElement getfirstleadid = locateElement("xpath", "(//a[@class='linktext'])[4]");
		String text = getText(getfirstleadid);
		//To click the first resulting lead
		WebElement clickingfirstlead = locateElement("xpath", "(//a[@class='linktext'])[4]");
		click(clickingfirstlead);
		//Clicking delete button
		WebElement delete = locateElement("link", "Delete");
		click(delete);
		//To click the findleads
		WebElement findleadslink = locateElement("xpath", "//a[text()='Find Leads']");
		click(findleadslink);
		WebElement enteringlead = locateElement("name", "id");
		type(enteringlead, text);
		//To click the find leads button
		WebElement leadsbutton = locateElement("xpath", " (//button[@class='x-btn-text'])[7]");
		click(leadsbutton);
		//To verify the error message
		WebElement error = locateElement("xpath", " //div[@class='x-paging-info']");
		getText(error);
		verifyExactText(error, "No records to display");
		//To close the browser
		//closeBrowser();











	}


}
