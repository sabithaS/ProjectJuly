package testNg;

import org.junit.Test;
import org.openqa.selenium.WebElement;

import wdMethods.SeMethods;

public class TC005_DuplicateLead extends SeMethods{
	@Test

	public void duplicatinglead() {

		startApp("chrome", "http://leaftaps.com/opentaps/control/main");

		WebElement username = locateElement("id", "username");	
		//to send the value for a element 
		type(username, "demosalesmanager");
		WebElement password = locateElement("id", "password");
		type(password, "crmsfa");
		//To click the login button
		WebElement login = locateElement("class", "decorativeSubmit");
		click(login);
		//to click the crmsfa link
		WebElement crmsfa = locateElement("link", "CRM/SFA");
		click(crmsfa);
		//to click the leads
		WebElement leads = locateElement("link", "Leads");
		click(leads);
		//to click the find lead link
		WebElement findleads = locateElement("link", "Find Leads");
		click(findleads);
		//to click on email field:
		WebElement email = locateElement("xpath", "//span[text()='Email']");
		click(email);
		//Enter a value in an email field
		WebElement emailvalue = locateElement("name", "emailAddress");
		type(emailvalue, "abc@def.com");
		//Click the findlead button
		WebElement findleadbutton = locateElement("xpath", " (//button[@class='x-btn-text'])[7]");
		click(findleadbutton);
		webdriverwait(findleadbutton);
		//capturing the name of the first resulting lead
		WebElement name = locateElement("link", "Vijay");
		String capturedexistingname = getText(name);
		//To click the first resulting lead
		WebElement firstresult = locateElement("xpath", "(//a[@class='linktext'])[4]");
		click(firstresult);
		//Clicking duplicate lead
		WebElement duplicateleadbutton = locateElement("class", "subMenuButton");
		click(duplicateleadbutton);
		//Getting the title
		String title = getTitle("Duplicate Lead");
		//Verifying the title
		verifyTitle("Duplicate Lead");
		//Clicking create lead
		WebElement createlead = locateElement("class", "smallSubmit");
		click(createlead);
		//Getting the name of duplicate lead
		WebElement verifyfirstname = locateElement("id", "viewLead_firstName_sp");
		getText(verifyfirstname);
		verifyExactText(verifyfirstname, capturedexistingname);
		//To close the browser
		closeBrowser();


	}


}
