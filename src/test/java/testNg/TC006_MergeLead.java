package testNg;


import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import wdMethods.SeMethods;

public class TC006_MergeLead extends Project_Methods {

	@BeforeClass

	public void testdata() {


		author="sabitha";
		category="Smoke";

		testCasename="TC006";
		testCaseDescription="Merge lead";


	}


	@Test(enabled=false)
	
	public void mergingLead() {

		/*startApp("chrome", "http://leaftaps.com/opentaps/control/main");

		WebElement username = locateElement("id", "username");	
		//to send the value for a element 
		type(username, "demosalesmanager");
		WebElement password = locateElement("id", "password");
		type(password, "crmsfa");
		//To click the login button
		WebElement login = locateElement("class", "decorativeSubmit");
		click(login);
		//to click the crmsfa link
		WebElement crmsfa = locateElement("link", "CRM/SFA");
		click(crmsfa);*/
		//login();
		//to click the leads
		WebElement leads = locateElement("link", "Leads");
		click(leads);
		
		//Click merge lead button
		WebElement mergeleadlink = locateElement("link", "Merge Leads");
		click(mergeleadlink);
		//Clicking the icon nearby from lead
		WebElement fromlead = locateElement("xpath", " (//img[@alt='Lookup'])[1]");
		click(fromlead);
		//Switching to new window
		getWindowHandle(1);


		//Entering lead id in new window
		WebElement leadid = locateElement("xpath", "//input[@name='id']");
		type(leadid, "10142");
		//Clicking find leads button in new window
		WebElement findleadsbutton = locateElement("xpath", "//button[text()='Find Leads']");
		click(findleadsbutton);
		webdriverwait(findleadsbutton);
		//Clicking first resulting lead in new window
		WebElement firstresult = locateElement("xpath", "(//a[@class='linktext'])[1]");
		clickWithNoSnap(firstresult);


		//Switching back to primarywindow
		getWindowHandle(0);

		//Clicking on icon near by To lead
		WebElement primarywindowimage = locateElement("xpath", " (//img[@alt='Lookup'])[2]");
		clickWithNoSnap(primarywindowimage);

		getWindowHandle(1);
		//validating in new window
		WebElement enteringleadid = locateElement("name", "id");
		type(enteringleadid, "10143");
		//clicking on find leads
		WebElement findleadsbutton1 = locateElement("class", "x-btn-text");
		click(findleadsbutton1);
		webdriverwait(findleadsbutton1);
		//clicking first resulting lead
		WebElement firstresultinglead = locateElement("xpath", "(//a[@class='linktext'])[1]");
		clickWithNoSnap(firstresultinglead);
		//Switching again back to primarywindow
		getWindowHandle(0);

		//Clicking on merge
		WebElement mergebutton = locateElement("link", "Merge");
		click(mergebutton);
		//webdriverwait(mergebutton);
		//Accepting an alert
		//acceptAlert();
		dismissAlert();
		//Clicking on find leads
		WebElement findleadslink = locateElement("link", "Find Leads");
		click(findleadslink);
		//Entering lead id
		WebElement leadid2 = locateElement("name", "id");
		type(leadid2, "10142");
		//Clicking on find leads
		WebElement findleadsbutton2 = locateElement("xpath", " (//button[@class='x-btn-text'])[7]");
		click(findleadsbutton2);
		webdriverwait(findleadsbutton2);
		//Verifying the error message
		WebElement error = locateElement("xpath", " //div[@class='x-paging-info']");
		getText(error);
		//Closing the browser
		closeBrowser();
	}

}
